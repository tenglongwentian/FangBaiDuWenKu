<?php
// +----------------------------------------------------------------------
// | Author: Bigotry <3162875@qq.com>
// +----------------------------------------------------------------------

namespace app\common\model;

/**
 * 会员模型
 */
class User extends ModelBase
{
    
	protected $insert = ['regtime'=>TIME_NOW];
    /**
     * 上级获取器
     */
    public function getLeaderNicknameAttr()
    {
        
        return model('User')->getValue(['id' => $this->data['leader_id']], 'nickname', '无');
    }
    
}
